var content='<div class="ui-page" deviceName="web" deviceType="desktop" deviceWidth="1440" deviceHeight="900">\
    <div id="t-f39803f7-df02-4169-93eb-7547fb8c961a" class="template growth-both devWeb canvas firer commentable non-processed" alignment="left" name="Template 1" width="1024" height="768">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/templates/f39803f7-df02-4169-93eb-7547fb8c961a-1677006825900.css" />\
      <div class="freeLayout">\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>\
    <div id="s-2769256d-14c5-4297-8759-364b515dffca" class="screen growth-vertical devWeb canvas PORTRAIT firer commentable non-processed" alignment="left" name="Login" width="1440" height="900">\
    <div id="backgroundBox"><div class="colorLayer"></div><div class="imageLayer"></div></div>\
    <div id="alignmentBox">\
      <link type="text/css" rel="stylesheet" href="./resources/screens/2769256d-14c5-4297-8759-364b515dffca-1677006825900.css" />\
      <div class="freeLayout">\
      <div id="s-Rectangle_2" class="rectangle manualfit firer commentable non-processed" customid="Background"   datasizewidth="1440.0px" datasizeheight="900.0px" datasizewidthpx="1440.0" datasizeheightpx="900.0" dataX="-0.0" dataY="0.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Rectangle_2_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Button_2" class="button multiline manualfit firer click mouseenter mouseleave commentable non-processed" customid="Sign Up Button"   datasizewidth="399.3px" datasizeheight="55.0px" dataX="177.4" dataY="497.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Button_2_0">Iniciar </span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Group_20" class="group firer ie-background commentable non-processed" customid="Password" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_19" class="group firer ie-background commentable non-processed" customid="Your-password" datasizewidth="183.0px" datasizeheight="46.0px" >\
          <div id="s-Input_3" class="password firer commentable non-processed" customid="Input search"  datasizewidth="407.0px" datasizeheight="58.0px" dataX="173.0" dataY="415.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="password"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div></div></div></div>\
          <div id="s-Path_27" class="path firer mouseenter mouseleave commentable non-processed" customid="Eye"   datasizewidth="21.3px" datasizeheight="13.3px" dataX="536.2" dataY="437.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="21.33465576171875" height="13.2916259765625" viewBox="536.2365591397853 437.0000000000001 21.33465576171875 13.2916259765625" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_27-27692" d="M546.9038978494627 437.00000000000017 C542.0551075268821 437.00000000000017 537.9142404557559 439.75579619368153 536.2365591397853 443.64581081081104 C537.9142404557559 447.53582486456816 542.0551075268821 450.2916216216218 546.9038978494627 450.2916216216218 C551.7526881720435 450.2916216216218 555.8935555514471 447.53582486456816 557.5712365591402 443.64581081081104 C555.8935555514471 439.75579619368153 551.7526881720435 437.00000000000017 546.9038978494627 437.00000000000017 Z M546.9038978494627 448.07635135135155 C544.2273653694381 448.07635135135155 542.0551075268821 446.0914688286311 542.0551075268821 443.64581081081104 C542.0551075268821 441.2001525113047 544.2273653694381 439.2152702702704 546.9038978494627 439.2152702702704 C549.5804297129316 439.2152702702704 551.7526881720435 441.2001525113047 551.7526881720435 443.64581081081104 C551.7526881720435 446.0914688286311 549.5804297129316 448.07635135135155 546.9038978494627 448.07635135135155 Z M546.9038978494627 440.9874864864866 C545.2940996103392 440.9874864864866 543.9946236559142 442.1748714865609 543.9946236559142 443.64581081081104 C543.9946236559142 445.11675013506107 545.2940996103392 446.30413513513525 546.9038978494627 446.30413513513525 C548.513696088586 446.30413513513525 549.8131720430113 445.11675013506107 549.8131720430113 443.64581081081104 C549.8131720430113 442.1748714865609 548.513696088586 440.9874864864866 546.9038978494627 440.9874864864866 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_27-27692" fill="#D1D1D1" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_4" class="richtext manualfit firer commentable non-processed" customid="Password"   datasizewidth="73.3px" datasizeheight="20.0px" dataX="192.7" dataY="405.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_4_0">Contrase&ntilde;a</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
\
      <div id="s-Group_21" class="group firer ie-background commentable non-processed" customid="Email" datasizewidth="0.0px" datasizeheight="0.0px" >\
        <div id="s-Group_7" class="group firer ie-background commentable non-processed" customid="Your-email" datasizewidth="183.0px" datasizeheight="46.0px" >\
          <div id="s-Input_2" class="text firer commentable non-processed" customid="Input search"  datasizewidth="407.0px" datasizeheight="58.0px" dataX="173.0" dataY="322.0" ><div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div><div class="borderLayer"><div class="paddingLayer"><div class="content"><div class="valign"><input type="text"  value="" maxlength="100"  tabindex="-1" placeholder=""/></div></div>  </div></div></div>\
          <div id="s-Path_25" class="path firer mouseenter mouseleave commentable non-processed" customid="ic_mail"   datasizewidth="20.8px" datasizeheight="15.2px" dataX="541.2" dataY="343.0"  >\
            <div class="borderLayer">\
            	<div class="imageViewport">\
              	<?xml version="1.0" encoding="UTF-8"?>\
              	<svg xmlns="http://www.w3.org/2000/svg" width="20.787628173828125" height="15.20001220703125" viewBox="541.1599462365584 342.999969482422 20.787628173828125 15.20001220703125" preserveAspectRatio="none">\
              	  <g>\
              	    <defs>\
              	      <path id="s-Path_25-27692" d="M543.2387096774187 342.999969482422 L559.8688172043004 342.999969482422 C561.0169191947539 342.999969482422 561.9475806451605 343.8505978482899 561.9475806451605 344.89996585846654 L561.9475806451605 356.29997220038865 C561.9475806451605 357.34924593109827 561.0169191947539 358.1999694824221 559.8688172043004 358.1999694824221 L543.2387096774187 358.1999694824221 C542.0906398400417 358.1999694824221 541.1599462365584 357.34924593109827 541.1599462365584 356.29997220038865 L541.1599462365584 344.89996585846654 C541.1599462365584 343.8505978482899 542.0906398400417 342.999969482422 543.2387096774187 342.999969482422 Z M551.5537634408595 349.64998533722735 L559.8688172043004 344.89996585846654 L543.2387096774187 344.89996585846654 L551.5537634408595 349.64998533722735 Z M543.2387096774187 356.29997220038865 L559.8688172043004 356.29997220038865 L559.8688172043004 347.1514614553607 L551.5537634408595 351.8906511961333 L543.2387096774187 347.1514614553607 L543.2387096774187 356.29997220038865 Z "></path>\
              	    </defs>\
              	    <g style="mix-blend-mode:normal">\
              	      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#s-Path_25-27692" fill="#D1D1D1" fill-opacity="1.0"></use>\
              	    </g>\
              	  </g>\
              	</svg>\
\
              </div>\
            </div>\
          </div>\
        </div>\
\
        <div id="s-Paragraph_7" class="richtext manualfit firer commentable non-processed" customid="Description"   datasizewidth="73.3px" datasizeheight="20.0px" dataX="192.7" dataY="312.0" >\
          <div class="backgroundLayer">\
            <div class="colorLayer"></div>\
            <div class="imageLayer"></div>\
          </div>\
          <div class="borderLayer">\
            <div class="paddingLayer">\
              <div class="content">\
                <div class="valign">\
                  <span id="rtr-s-Paragraph_7_0">Correo</span>\
                </div>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_3" class="richtext manualfit firer ie-background commentable non-processed" customid="Login your account"   datasizewidth="324.4px" datasizeheight="47.0px" dataX="203.9" dataY="233.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_3_0">Ingresa en </span><span id="rtr-s-Paragraph_3_1">Ganado&iexcl;Ya!</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
      <div id="s-Paragraph_2" class="richtext manualfit firer mouseenter mouseleave ie-background commentable non-processed" customid="Start your personal photo experience"   datasizewidth="367.6px" datasizeheight="25.0px" dataX="182.3" dataY="192.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_2_0"></span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_1" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="720.0px" datasizeheight="900.0px" dataX="720.0" dataY="-0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/ba25017b-b49f-4d63-9b83-63af065343cd.jpg" />\
        	</div>\
        </div>\
      </div>\
\
\
      <div id="s-Image_2" class="image firer ie-background commentable non-processed" customid="Image 2"   datasizewidth="177.0px" datasizeheight="204.4px" dataX="270.5" dataY="17.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/0a537c47-1a7f-4139-9e33-437dc0c589d5.png" />\
        	</div>\
        </div>\
      </div>\
\
      <div id="s-Paragraph_1" class="richtext manualfit firer click mouseenter mouseleave ie-background commentable non-processed" customid="No tienes cuenta? Registr"   datasizewidth="243.0px" datasizeheight="53.0px" dataX="255.0" dataY="623.0" >\
        <div class="backgroundLayer">\
          <div class="colorLayer"></div>\
          <div class="imageLayer"></div>\
        </div>\
        <div class="borderLayer">\
          <div class="paddingLayer">\
            <div class="content">\
              <div class="valign">\
                <span id="rtr-s-Paragraph_1_0">No tienes cuenta?</span><span id="rtr-s-Paragraph_1_1"> </span><span id="rtr-s-Paragraph_1_2">Registrate</span>\
              </div>\
            </div>\
          </div>\
        </div>\
      </div>\
\
      <div id="s-Image_3" class="image firer ie-background commentable non-processed" customid="Image 1"   datasizewidth="720.0px" datasizeheight="900.0px" dataX="720.0" dataY="-0.0"   alt="image">\
        <div class="borderLayer">\
        	<div class="imageViewport">\
        		<img src="./images/ba25017b-b49f-4d63-9b83-63af065343cd.jpg" />\
        	</div>\
        </div>\
      </div>\
\
      </div>\
\
      </div>\
      <div id="loadMark"></div>\
    </div>  \
</div>\
';
document.getElementById("chromeTransfer").innerHTML = content;