(function(window, undefined) {
  var dictionary = {
    "1d4e7901-ed97-45b9-82b9-471f00278d2b": "Perfil",
    "5c012e76-cec3-404e-9fd8-6eada73bc01e": "Carrito",
    "efdb7ef4-1620-471b-823c-5a14e4a90cf8": "Comprar",
    "afd380a2-20da-4ffe-be93-a2e5307375fb": "Home",
    "d206744a-6453-4591-9535-d575284509b1": "Registro",
    "2769256d-14c5-4297-8759-364b515dffca": "Login",
    "36d90a29-4b73-4539-a77e-354e7cf0f6c1": "Detalles",
    "79089a14-25cc-42e9-9259-92f36c3c2d2e": "ActualizarDatos",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);