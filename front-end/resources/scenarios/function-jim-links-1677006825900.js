(function(window, undefined) {

  var jimLinks = {
    "1d4e7901-ed97-45b9-82b9-471f00278d2b" : {
      "Paragraph_12" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_1" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_2" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Image_1" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Button_1" : [
        "79089a14-25cc-42e9-9259-92f36c3c2d2e"
      ]
    },
    "5c012e76-cec3-404e-9fd8-6eada73bc01e" : {
      "Image_3" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_12" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_6" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Paragraph_1" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_2" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_3" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Button_2" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ]
    },
    "efdb7ef4-1620-471b-823c-5a14e4a90cf8" : {
      "Image_3" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_6" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Paragraph_2" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_3" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Rectangle_1" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Rectangle_5" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Rectangle_6" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Image_1" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Image_4" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Image_5" : [
        "36d90a29-4b73-4539-a77e-354e7cf0f6c1"
      ],
      "Image_6" : [
        "5c012e76-cec3-404e-9fd8-6eada73bc01e"
      ]
    },
    "afd380a2-20da-4ffe-be93-a2e5307375fb" : {
      "Paragraph_12" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_6" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Paragraph_1" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Paragraph_3" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Paragraph_7" : [
        "efdb7ef4-1620-471b-823c-5a14e4a90cf8"
      ],
      "Image_4" : [
        "5c012e76-cec3-404e-9fd8-6eada73bc01e"
      ]
    },
    "d206744a-6453-4591-9535-d575284509b1" : {
      "Button_2" : [
        "2769256d-14c5-4297-8759-364b515dffca"
      ]
    },
    "2769256d-14c5-4297-8759-364b515dffca" : {
      "Button_2" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_1" : [
        "d206744a-6453-4591-9535-d575284509b1"
      ]
    },
    "36d90a29-4b73-4539-a77e-354e7cf0f6c1" : {
      "Image_3" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_6" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Paragraph_2" : [
        "afd380a2-20da-4ffe-be93-a2e5307375fb"
      ],
      "Paragraph_3" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ],
      "Button_1" : [
        "5c012e76-cec3-404e-9fd8-6eada73bc01e"
      ],
      "Image_4" : [
        "5c012e76-cec3-404e-9fd8-6eada73bc01e"
      ]
    },
    "79089a14-25cc-42e9-9259-92f36c3c2d2e" : {
      "Button_2" : [
        "1d4e7901-ed97-45b9-82b9-471f00278d2b"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);