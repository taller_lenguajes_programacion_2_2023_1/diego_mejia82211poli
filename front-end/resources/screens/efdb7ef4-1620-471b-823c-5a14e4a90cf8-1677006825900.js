jQuery("#simulation")
  .on("click", ".s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Image_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/afd380a2-20da-4ffe-be93-a2e5307375fb",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1d4e7901-ed97-45b9-82b9-471f00278d2b",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/afd380a2-20da-4ffe-be93-a2e5307375fb",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1d4e7901-ed97-45b9-82b9-471f00278d2b",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/36d90a29-4b73-4539-a77e-354e7cf0f6c1",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/5c012e76-cec3-404e-9fd8-6eada73bc01e",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Image_3") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_3 svg" ],
                    "attributes": {
                      "path-filter": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><filter xmlns=\"http://www.w3.org/2000/svg\" color-interpolation-filters=\"sRGB\">  <feOffset dx=\"7.071067811865475\" dy=\"7.0710678118654755\" input=\"SourceAlpha\"></feOffset>  <feGaussianBlur result=\"DROP_SHADOW_0_blur\" stdDeviation=\"5\"></feGaussianBlur>  <feFlood flood-color=\"#404040\" flood-opacity=\"1.0\"></feFlood>  <feComposite operator=\"in\" in2=\"DROP_SHADOW_0_blur\"></feComposite>  <feComposite in=\"SourceGraphic\" result=\"DROP_SHADOW_0\"></feComposite></filter>"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_3" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #shapewrapper-s-Ellipse_2 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#0DE578"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_6") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_6 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "19.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_1") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_7 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_7 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FFFFFF"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "16.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_1") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_7 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#FD9727",
                      "border-right-color": "#FD9727",
                      "border-bottom-color": "#FD9727",
                      "border-left-color": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_4 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "16.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "18.0pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_1 svg" ],
                    "attributes": {
                      "path-filter": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><filter xmlns=\"http://www.w3.org/2000/svg\" color-interpolation-filters=\"sRGB\">  <feOffset dx=\"7.071067811865475\" dy=\"7.0710678118654755\" input=\"SourceAlpha\"></feOffset>  <feGaussianBlur result=\"DROP_SHADOW_0_blur\" stdDeviation=\"5\"></feGaussianBlur>  <feFlood flood-color=\"#404040\" flood-opacity=\"1.0\"></feFlood>  <feComposite operator=\"in\" in2=\"DROP_SHADOW_0_blur\"></feComposite>  <feComposite in=\"SourceGraphic\" result=\"DROP_SHADOW_0\"></feComposite></filter>"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_1" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_4") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_4 svg" ],
                    "attributes": {
                      "path-filter": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><filter xmlns=\"http://www.w3.org/2000/svg\" color-interpolation-filters=\"sRGB\">  <feOffset dx=\"7.071067811865475\" dy=\"7.0710678118654755\" input=\"SourceAlpha\"></feOffset>  <feGaussianBlur result=\"DROP_SHADOW_0_blur\" stdDeviation=\"5\"></feGaussianBlur>  <feFlood flood-color=\"#404040\" flood-opacity=\"1.0\"></feFlood>  <feComposite operator=\"in\" in2=\"DROP_SHADOW_0_blur\"></feComposite>  <feComposite in=\"SourceGraphic\" result=\"DROP_SHADOW_0\"></feComposite></filter>"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_4" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_8 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_8 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#FD9727",
                      "border-right-color": "#FD9727",
                      "border-bottom-color": "#FD9727",
                      "border-left-color": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_8 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "16.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_9 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "18.0pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_5") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_5 svg" ],
                    "attributes": {
                      "path-filter": "<?xml version=\"1.0\" encoding=\"UTF-8\"?><filter xmlns=\"http://www.w3.org/2000/svg\" color-interpolation-filters=\"sRGB\">  <feOffset dx=\"7.071067811865475\" dy=\"7.0710678118654755\" input=\"SourceAlpha\"></feOffset>  <feGaussianBlur result=\"DROP_SHADOW_0_blur\" stdDeviation=\"5\"></feGaussianBlur>  <feFlood flood-color=\"#404040\" flood-opacity=\"1.0\"></feFlood>  <feComposite operator=\"in\" in2=\"DROP_SHADOW_0_blur\"></feComposite>  <feComposite in=\"SourceGraphic\" result=\"DROP_SHADOW_0\"></feComposite></filter>"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Image_5" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_9 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Rectangle_9 > .borderLayer" ],
                    "attributes": {
                      "border-top-color": "#FD9727",
                      "border-right-color": "#FD9727",
                      "border-bottom-color": "#FD9727",
                      "border-left-color": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_10 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "16.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Paragraph_11 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "18.0pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_6") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #shapewrapper-s-Ellipse_1" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #s-Ellipse_1" ],
                    "attributes": {
                      "stroke-width": "6.0px",
                      "stroke": "#FD9727"
                    }
                  },{
                    "target": [ "#s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-efdb7ef4-1620-471b-823c-5a14e4a90cf8 .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Image_3")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Paragraph_6")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Rectangle_1")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_1")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_4")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_5")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_6")) {
      jEvent.undoCases(jFirer);
    }
  });