jQuery("#simulation")
  .on("click", ".s-afd380a2-20da-4ffe-be93-a2e5307375fb .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Paragraph_12")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/efdb7ef4-1620-471b-823c-5a14e4a90cf8",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1d4e7901-ed97-45b9-82b9-471f00278d2b",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/efdb7ef4-1620-471b-823c-5a14e4a90cf8",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_3")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/1d4e7901-ed97-45b9-82b9-471f00278d2b",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/efdb7ef4-1620-471b-823c-5a14e4a90cf8",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/5c012e76-cec3-404e-9fd8-6eada73bc01e",
                    "transition": {
                      "type": "fade",
                      "duration": 500
                    }
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-afd380a2-20da-4ffe-be93-a2e5307375fb .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Paragraph_12") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_12 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "19.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_6") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_6 span" ],
                    "attributes": {
                      "color": "#FD9727",
                      "font-size": "19.5pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Paragraph_7") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "6.0px",
                      "border-top-right-radius": "6.0px",
                      "border-bottom-right-radius": "6.0px",
                      "border-bottom-left-radius": "6.0px"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#EC9C45"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7 > .borderLayer" ],
                    "attributes": {
                      "border-top-left-radius": "6.0px",
                      "border-top-right-radius": "6.0px",
                      "border-bottom-right-radius": "6.0px",
                      "border-bottom-left-radius": "6.0px"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7 > .backgroundLayer" ],
                    "attributes": {
                      "border-top-left-radius": "6.0px",
                      "border-top-right-radius": "6.0px",
                      "border-bottom-right-radius": "6.0px",
                      "border-bottom-left-radius": "6.0px"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Paragraph_7 span" ],
                    "attributes": {
                      "color": "#FEE94E",
                      "font-size": "27.0pt"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Line_3" ],
                    "attributes": {
                      "stroke": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Line_4" ],
                    "attributes": {
                      "stroke": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Line_1" ],
                    "attributes": {
                      "stroke": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        },
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Line_2" ],
                    "attributes": {
                      "stroke": "#FD9727"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_4") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #shapewrapper-s-Ellipse_1" ],
                    "attributes": {
                      "filter": " drop-shadow(7.071067811865475px 7.0710678118654755px 5px #404040)"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #s-Ellipse_1" ],
                    "attributes": {
                      "stroke-width": "6.0px",
                      "stroke": "#FD9727"
                    }
                  },{
                    "target": [ "#s-afd380a2-20da-4ffe-be93-a2e5307375fb #shapewrapper-s-Ellipse_1 > .backgroundLayer > .colorLayer" ],
                    "attributes": {
                      "background-color": "#FEE94E"
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-afd380a2-20da-4ffe-be93-a2e5307375fb .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(jimUtil.isAlternateModeActive()) return;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Paragraph_12")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Paragraph_6")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Paragraph_7")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_4")) {
      jEvent.undoCases(jFirer);
    }
  });